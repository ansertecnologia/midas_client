# MidasClient
Midas Client its a Ruby GEM that's allow user to send requests to Anser Payment API

https://ansertecnologia.atlassian.net/wiki/x/EYCyAg

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'midas_client', git: 'https://bitbucket.org/ansertecnologia/midas_client.git'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install midas_client

## Usage
Credentials for Sandbox:

- user: pdv01
- password: /eFRsg8RLne+I0GlECQofYvnsDfb0w==

For details go to API Documentation
[https://ansertecnologia.atlassian.net/wiki/x/EYCyAg]
 or visit Anser Gateway site at [https://ansertenologia.com]

Use client as follow:

Request

    $ request = MidasClient::Request.new('testes-01','/eFRsg8RLne+I0GlECQofYvnsDfb0w==', 'DEVELOPMENT')
    $ request.transaction.card_store({"externalId":"teste_midas_client_00002", "pan":"5529525900844124", "expirationMonth":"01", "expirationYear":"2021", "holderName":"TESTE MIDAS CLIENT", "customer":{ "documentType":"CPF", "documentNumber":"36549450152"}})
    $ request.subscription.cancel_subscription("subscription_token_00002")
    $ request.query.list_customers

Response

    $ [17/05/2017 20:48:05.087]METHOD: post URL: https://sandbox.ansertecnologia.net/midas-core/v2/creditcard login: pdv01 password: /eFRsg8RLne+I0GlECQofYvnsDfb0w== payload: {:externalId=>"teste_midas_client_00002", :pan=>"552952@@@@@@4124", :expirationMonth=>"01", :expirationYear=>"2021", :holderName=>"TESTE MIDAS CLIENT", :customer=>{:documentType=>"CPF", :documentNumber=>"36549450152"}}
    $ [17/05/2017 20:48:06.103][RESPONSE] {:result=>{:success=>true, :code=>"010", :message=>"O cartao jah foi armazenado anteriormente"}, :brand=>"MASTER", :cardToken=>"f887d732863eb3ef2e45c0bd8b57ed33"} 

## Change Releases
VERSION - 0.2.13a - 2018/07/23

- changing MidasClient::Endpopoints - Sandbox endpoint has changed to 

VERSION - 0.2.13 - 2018/07/12

- changing MidasClient::Endpopoints

VERSION - 0.2.12 - 2018/06/04

- Operation MidasClient::Subscription.invoice_external_payment(invoice_token, payment_date, payment_voucher) - associate an external payment to an invoice
- Removing operation MidasClient::Subscription.subscription_transactions(subscription_token)

VERSION - 0.2.11 - 2018/05/11

- Operation MidasClient::Management.list_pos - removal of param 'auth', now uses management_auth from env endpoints
- Operation MidasClient::Management.create_pos - removal of param 'auth', now uses management_auth from env endpoints
- Operation MidasClient::Management.pos_details - removal of param 'auth', now uses management_auth from env endpoints

VERSION - 0.2.10 - 2018/03/30

- Operation MidasClient::Subscription.invoice_cancel(token) - cancel a subscription's invoice.


VERSION - 0.2.9 - 2018/03/20

- Operation MidasClient::Subscription.renew(token, params) - renew a subscription increasing the number of invoices associated.
- Operation MidasClient::Transaction.create_billet(params) - create a billet to be payed.
- Operation MidasClient::Transaction.print_billet(token) - print a billet to be payed.

VERSION - 0.2.8 - 2018/02/06

- Operation MidasClient::Management.pos_details(midas_login) to return a POS' details like list of card brands allowed
- Operation MidasClient::Query.invoices_by_expiration_date(start_date, end_date, status) to return all invoices by expiration date
- Operation MidasClient::Query.invoices_by_payment_date(start_date, end_date, status) to return all invoices by payment date

VERSION - 0.2.7 - 2018/01/25

- Improving MidasClient::Subscription.create to create a manual subscription when de cardToken is not informed
- Implementing an invoice payment operation - MidasClient::Subscription.invoice_payment(invoice_token, transaction_token)
- Query to summarizes all card's transactions by date - MidasClient::Query.cards_summary_by_day(start_date, end_date)
- Query to summarizes all card's transactions by month - MidasClient::Query.cards_summary_by_month(start_month, start_year, end_month, end_year)
- Query to summarizes all billet's transactions by date - MidasClient::Query.cards_summary_by_day(start_date, end_date)
- Query to summarizes all billet's transactions by month - MidasClient::Query.cards_summary_by_month(start_month, start_year, end_month, end_year)


VERSION - 0.2.6 - 2017/11/17

- Query to list all transactions from an invoice's token - MidasClient::Subscription.invoice_transactions(invoiceToken)
- Query to list all transactions from a subscription's token - MidasClient::Subscription.subscription_transactions(subscriptionToken)


VERSION - 0.2.5 - 2017/11/11

- Query to list transactions from an user's document - MidasClient::Query.transactions_by_customer(document_type, document_number, status)
- Query to list subscriptions from an user's document - MidasClient::Query.subscriptions_by_customer(document_type, document_number, status)
- Query to list invoices from a Point of Sale - MidasClient::Query.invoices(start_date, end_date, status))


VERSION - 0.2.4 - 2017/11/01

- Refactoring QUERIES CONSTANT to QUERY
- Query to list creditcards from a Point of Sale (MidasClient::Query.list_creditcards)
- Query to list creditcards from user's document number (MidasClient::Transation.query_user_cards)
- Query to list transactions by an array of external ids (MidasClient::Query.by_external_ids)
- Query to list transactions by an array of transaction tokens (MidasClient::Query.by_transaction_tokens)
- Query to list customers from  de POint of Sale (MidasClient::Query.list_customers)
- Access attributes at Request to acess all subclasses from Request as singleton methods
- Refactoring class Queries name to Query
- read.me

VERSION - 0.2.3 - 2017/10/25

- Bug fix at MidasClient::Subscription.subscription_update
- read.me

VERSION - 0.2.2 - 2017/10/16

- Query to return all subscritions for a Point Of Sale at module Query.
- read.me

VERSION - 0.2.1 - 2017/10/11

- Changing Development API endpoint
- read.me

VERSION - 0.2.0 - 2017/08/26

- Bugfix for queries
- Changing returns from Array to Hash (json)
- release up
- read.me

VERSION - 0.1.4.2 - 2017/08/23

- Bugfix for set_environment
- Initial model for Billet operations (Boleto)
- read.me

VERSION - 0.1.4.1 - 2017/07/12

- Function set_environment to force which environment call
- Function query_by_external_id to list all transactions related to an external_id
- read.me

VERSION - 0.1.4 - 2017/06/01

- List POS operation
- Create a new POS
- Bug fix at MidasClient::Request.external_request method
- read.me

VERSION - 0.1.3.1 - 2017/05/28

- publishing stable gem
- read.me

VERSION - 0.1.3 - 2017/05/22

- Removing Rails log dependency
- Removing Rails.env dependency
- read.me

VERSION - 0.1.2 - 2017/05/19

- Update Subscription
- Update Invoice
- bugfix
- read.me

VERSION - 0.1.1 - 2017/05/16

- Implementing logging features
- bugfix

VERSION - 0.1.0 - 2017/05/15

- Creditcard Store (tokenization)
- Synchronous Transaction (Authorize + Capture)
- Asynchronous Transaction (Authorize + Capture)
- Asynchronous Debit Transaction
- Authorize (synchronous)
- Capture
- Cancellation
- Refund
- Query Transaction
- Create Subscription
- Cancel Subscription
- Subscription by Token
- List Subscription's Invoices
- Update Subscription Card

## Contributing

- Tests must be implemented, please pull request tests
- Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/midas_client. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the MidasClient project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/midas_client/blob/master/CODE_OF_CONDUCT.md).
