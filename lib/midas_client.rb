require 'rubygems'
require 'stringio'
require 'logger'
require 'rest_client'
require 'credit_card_sanitizer'

require_relative "midas_client/version"
require_relative "midas_client/util"
require_relative "midas_client/endpoints"
require_relative "midas_client/request"
require_relative "midas_client/transaction"
require_relative "midas_client/subscription"
require_relative "midas_client/query"
require_relative "midas_client/management"


#Dir[File.expand_path('../midas_client/resources/*.rb', __FILE__)].map do |path|
#  require path
#end

module MidasClient

  class << self
    attr_reader :gem_root

    spec = Gem::Specification.find_by_name("midas_client")
    @gem_root = spec.try(:gem_dir)

  end
end
