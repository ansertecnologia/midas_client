module MidasClient
    class Management
      include Util
      include RestClient

      #= This method LIST all POS in MIDAS.
      # This is a is synchronous operation, using method GET
      #
      # Params:
      #   auth: Base 64 authorization token
      #   env: Base 64 authorization token
      #
      #
      # Response:
      # {
      # "result": {
      #    "success": true,
      #    "code": "000",
      #    "message": "Sucesso"
      # },
      #    "pointsOfSale": [
      #    {
      #        "code": "pdv01",
      #        "description": "PDV de Teste 1 (Cielo)"
      #    },
      #    {
      #        "code": "pdv02",
      #
      #        "description": "PDV de Teste 2 (Stone)"
      #    }]
      # }
      #
      def self.list_pos(environment='DEVELOPMENT')
        # define o método de envio da requisição
        method = :get

        # define o ambiente de execução
        _env = environment.upcase == 'PRODUCTION' ? EndPoints::PRODUCTION : EndPoints::DEVELOPMENT

        # monta a URL de chamada da requisição
        endpoint =  _env[:url] + EndPoints::MANAGEMENTS[:context] + EndPoints::MANAGEMENTS[:list_pos]

        # monta authorization base64 no header
        #headers={content_type: :json, accept: :json, authorization: get_env[:management_auth]}
        headers={content_type: :json, accept: :json, authorization: _env[:management_auth]}

        # faz a chamada a plataforma de pagamento (MIDAS)
        Request.external_request(method, endpoint, {}, headers)

      end

      #= This method creates an POS in midas.
      # This is a is synchronous operation, using method POST
      #
      # Params:
      #   auth: Base 64 authorization token
      #   code: string - Represents the name of the POS
      #   description: string - Description about the POS
      #
      #
      # Response:
      #   result: {
      #     success: true/false
      #     code: "XXX"
      #     message: "Some message to you"
      #   },
      #   accessToken: "3Lpv2ecaeBa4Kffuf0CbWfF1j6I5eg=="
      #
      def self.create_pos(code, description, environment= 'DEVELOPMENT')
        # define o método de envio da requisição
        method = :post

        # define o ambiente de execução
        _env = environment.upcase == 'PRODUCTION' ? EndPoints::PRODUCTION : EndPoints::DEVELOPMENT

        # monta a URL de chamada da requisição
        endpoint =  _env[:url] + EndPoints::MANAGEMENTS[:context] + EndPoints::MANAGEMENTS[:create_pos]

        # monta os parâmetros da requisição na url
        params = { code: code, description: description}

        # monta authorization base64 no header
        #headers={content_type: :json, accept: :json, authorization: get_env[:management_auth]}
        headers={content_type: :json, accept: :json, authorization: _env[:management_auth]}

        # faz a chamada a plataforma de pagamento (MIDAS)
        Request.external_request(method, endpoint, params, headers)

      end

      # retorna os detalhes de um PDV
      def self.pos_details(midas_login, environment= 'DEVELOPMENT')
        # define o método de envio da requisição
        method = :get

        # define o ambiente de execução
        _env = environment.upcase == 'PRODUCTION' ? EndPoints::PRODUCTION : EndPoints::DEVELOPMENT

        # monta a URL de chamada da requisição
        endpoint =  _env[:url] + EndPoints::MANAGEMENTS[:context] + EndPoints::MANAGEMENTS[:pos_details].gsub("{midasLogin}", midas_login)

        # monta os parâmetros da requisição na url
        params = { }

        # monta authorization base64 no header
        headers={content_type: :json, accept: :json, authorization: _env[:management_auth]}

        # faz a chamada a plataforma de pagamento (MIDAS)
        Request.external_request(method, endpoint, params, headers)
      end
    end
end