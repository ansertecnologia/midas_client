module MidasClient
  module Util

    attr_accessor :logger

    def logger(progname = '[MIDAS_CLIENT]')
      @logger ||= Logger.new($stdout).tap do |log|
        log.progname = progname
      end
    end

    def log(text)
      logger.level = Logger::INFO
      logger.info "#{text}"
    end

    def error_log(text)
      logger.level = Logger::ERROR
      logger.error "#{text}"
    end

    def sanitized_document_number(number)
      number.gsub('.', '').gsub('-','') unless number.nil?
    end

    def sanitize_pci(text)
      CreditCardSanitizer.new(replacement_token: '@').sanitize!(text.to_s) || text
    end
  end
end