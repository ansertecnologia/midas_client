module MidasClient
  class Query < Request

      #= This method performs a query by a range date.
      # This is a is synchronous operation, using method GET
      #
      # Params:
      #   start_date: date format YYYY-MM-DD
      #   send_date: date format YYYY-MM-DD
      #   status: string
      #   type: string DIRECT/RECURRENT
      #
      # Response:
      #   result: {
      #     success: true/false
      #     code: "XXX"
      #     message: "Some message to you"
      #   }
      def transaction_by_date(start_date=(Date.today - 7).strftime('%Y-%m-%d'), end_date = Date.today.strftime('%Y-%m-%d'), status = nil, type=nil)
          log "Entrei em transaction_by_date"
          # define o método de envio da requisição
          method = :get

          # monta a URL de chamada da requisição
          endpoint =  get_env[:url] + EndPoints::QUERIES[:context] + EndPoints::QUERIES[:by_period]
          endpoint =  endpoint.gsub("{startDate}", start_date).gsub("{endDate}", end_date)

          # monta o parâmetro status na requisição da url
          endpoint = status.blank? ? endpoint.gsub("&status={status}", '') : endpoint.gsub("{status}", status)

          # monta o parâmetro type na requisição da url
          endpoint = type.blank? ? endpoint.gsub("&type={type}", '') : endpoint.gsub("{type}", type)

          # faz a chamada a plataforma de pagamento (MIDAS)
          response = request(method, endpoint, self.login, self.password, {})

          result = response[:result]
          pagging = response[:pagging]
          if response[:transactions].kind_of?(Array) || response[:transactions].blank?
            transactions = response[:transactions]
          else
            transaction_array = []
            transaction_array << response[:transactions]
            transactions = transaction_array
          end

          response[:result] = result
          response[:pagging] = pagging
          response[:transactions] = transactions

          response
      end

      #= This method performs a query by a specific transaction's identifier, called external ID.
      # This is a is synchronous operation, using method GET
      #
      # Params:
      #   transactionToken: string (Transaction unique identification generated by customer)
      #
      #
      # Response:
      #   result: {
      #     success: true/false
      #     code: "XXX"
      #     message: "Some message to you"
      #   }
      def by_external_id(externalId)
        log "Entrei em by_external_id"

        # define o método de envio da requisição
        method = :get

        # monta a URL de chamada da requisição
        endpoint =  get_env[:url] + EndPoints::QUERIES[:context] + EndPoints::QUERIES[:by_external_id].gsub('{externalId}', externalId)

        request(method, endpoint, login, password, {})
      end

      #= This method performs a query by an array of transaction's identifier.
      # This is a is synchronous operation, using method GET
      #
      # Params:
      #   none
      #
      #
      # Response:
      #   result: {
      #     success: true/false
      #     code: "XXX"
      #     message: "Some message to you"
      #   }
      def by_external_ids(externalIds = [])
        log "Entrei em by_external_ids"

        # define o método de envio da requisição
        method = :post

        # monta a URL de chamada da requisição
        endpoint =  get_env[:url] + EndPoints::QUERIES[:context] + EndPoints::QUERIES[:by_external_ids]

        request(method, endpoint, login, password, {externalIds: externalIds})
      end

      #= This method performs a query by an array of transaction's token.
      # This is a is synchronous operation, using method GET
      #
      # Params:
      #   none
      #
      #
      # Response:
      #   result: {
      #     success: true/false
      #     code: "XXX"
      #     message: "Some message to you"
      #   }
      def by_transaction_tokens(transactionTokens = [])
        log "Entrei em by_external_ids"

        # define o método de envio da requisição
        method = :post

        # monta a URL de chamada da requisição
        endpoint =  get_env[:url] + EndPoints::QUERIES[:context] + EndPoints::QUERIES[:by_transaction_tokens]

        request(method, endpoint, login, password, {transactionTokens: transactionTokens})
      end

      #= This method performs a query to return all subscriptions for a Point Of Sale by status
      # This is a is synchronous operation, using method GET
      #
      # Params:
      #   status: ACTIVE/CANCELLED
      #
      # Response:
      #   result: {
      #     success: true/false
      #     code: "XXX"
      #     message: "Some message to you"
      #   }
      def subscriptions(status = nil)
        log "Entrei em subscriptions"
        # define o método de envio da requisição
        method = :get

        # monta a URL de chamada da requisição
        endpoint =  get_env[:url] + EndPoints::QUERIES[:context] + EndPoints::QUERIES[:subscriptions]

        # monta os parâmetros da requisição na url
        endpoint = status.blank? ? endpoint.gsub("?status={status}", '') : endpoint.gsub("{status}", status)

        # faz a chamada a plataforma de pagamento (MIDAS)
        response = request(method, endpoint, self.login, self.password, {})

        result = response[:result]
        pagging = response[:pagging]
        if response[:subscriptions].kind_of?(Array) || response[:subscriptions].blank?
          subscriptions = response[:subscriptions]
        else
          subscription_array = []
          subscription_array << response[:subscriptions]
          subscriptions = subscription_array
        end

        response[:result] = result
        response[:pagging] = pagging
        response[:subscriptions] = subscriptions

        response
      end

      #= This method performs a query to return all invoices for a Point Of Sale by a period (by expirationDate) and status
      # This is a is synchronous operation, using method GET
      #
      # Params:
      #   start_date: YYYY-MM-DD
      #   end_date: YYYY-MM-DD
      #   status: PAID/DENIED/RESERVED/CANCELLED
      #
      # Response:
      #   result: {
      #     success: true/false
      #     code: "XXX"
      #     message: "Some message to you"
      #   }
      def invoices_by_expiration_date(start_date=(Date.today - 7).strftime('%Y-%m-%d'), end_date = Date.today.strftime('%Y-%m-%d'), status = 'PAID')
        log "Entrei em invoices"
        # define o método de envio da requisição
        method = :get

        # monta a URL de chamada da requisição
        endpoint =  get_env[:url] + EndPoints::QUERIES[:context] + EndPoints::QUERIES[:invoices_by_expiration_date]

        # monta a URL de chamada da requisição
        endpoint =  endpoint .gsub("{startDate}", start_date).gsub("{endDate}", end_date)

        # monta o parâmetro status na requisição da url
        endpoint = status.blank? ? endpoint.gsub("&status={status}", '') : endpoint.gsub("{status}", status)

        # faz a chamada a plataforma de pagamento (MIDAS)
        response = request(method, endpoint, self.login, self.password, {})

        result = response[:result]
        pagging = response[:pagging]
        if response[:invoices].kind_of?(Array) || response[:invoices].blank?
          invoices = response[:invoices]
        else
          invoice_array = []
          invoice_array << response[:invoices]
          invoices = invoice_array
        end

        response[:result] = result
        response[:pagging] = pagging
        response[:invoices] = invoices

        response
      end

      #= This method performs a query to return all invoices for a Point Of Sale by a period (by paymentDate) and status
      # This is a is synchronous operation, using method GET
      #
      # Params:
      #   start_date: YYYY-MM-DD
      #   end_date: YYYY-MM-DD
      #   status: PAID/DENIED/RESERVED/CANCELLED
      #
      # Response:
      #   result: {
      #     success: true/false
      #     code: "XXX"
      #     message: "Some message to you"
      #   }
      def invoices_by_payment_date(start_date=(Date.today - 7).strftime('%Y-%m-%d'), end_date = Date.today.strftime('%Y-%m-%d'), status = 'PAID')
        log "Entrei em invoices"
        # define o método de envio da requisição
        method = :get

        # monta a URL de chamada da requisição
        endpoint =  get_env[:url] + EndPoints::QUERIES[:context] + EndPoints::QUERIES[:invoices_by_payment_date]

        # monta a URL de chamada da requisição
        endpoint =  endpoint .gsub("{startDate}", start_date).gsub("{endDate}", end_date)

        # monta o parâmetro status na requisição da url
        endpoint = status.blank? ? endpoint.gsub("&status={status}", '') : endpoint.gsub("{status}", status)

        # faz a chamada a plataforma de pagamento (MIDAS)
        response = request(method, endpoint, self.login, self.password, {})

        result = response[:result]
        pagging = response[:pagging]
        if response[:invoices].kind_of?(Array) || response[:invoices].blank?
          invoices = response[:invoices]
        else
          invoice_array = []
          invoice_array << response[:invoices]
          invoices = invoice_array
        end

        response[:result] = result
        response[:pagging] = pagging
        response[:invoices] = invoices

        response
      end

      #= This method performs a query to return all creditcards stored for a Point Of Sale
      # This is a is synchronous operation, using method GET
      #
      # Params:
      #   none
      #
      # Response:
      #   result: {
      #     success: true/false
      #     code: "XXX"
      #     message: "Some message to you"
      #   }
      #   creditCards: [
      #   {
      #      brand: "MASTER",
      #      panLastDigits": "4832",
      #      expirationMonth": 10,
      #      expirationYear": 2019,
      #      holderName": "Nome Portador",
      #      customer: {
      #         documentType: "CPF",
      #         documentNumber: "12345678900"
      #      },
      #      token: "b7553c62bc93ed0708b4behfcf28f3592"
      #    }
      def list_creditcards()
        log "Entrei em list_creditcards"
        # define o método de envio da requisição
        method = :get

        # monta a URL de chamada da requisição
        endpoint =  get_env[:url] + EndPoints::QUERIES[:context] + EndPoints::QUERIES[:creditcards]

        # faz a chamada a plataforma de pagamento (MIDAS)
        request(method, endpoint, self.login, self.password, {})
      end

      #= This method performs a query to return all creditcards stored for a Point Of Sale
      # This is a is synchronous operation, using method GET
      #
      # Params:
      #   none
      #
      # Response:
      #   result: {
      #     success: true/false
      #     code: "XXX"
      #     message: "Some message to you"
      #   }
      #   creditCards: [
      #   {
      #      brand: "MASTER",
      #      panLastDigits": "4832",
      #      expirationMonth": 10,
      #      expirationYear": 2019,
      #      holderName": "Nome Portador",
      #      customer: {
      #         documentType: "CPF",
      #         documentNumber: "12345678900"
      #      },
      #      token: "b7553c62bc93ed0708b4behfcf28f3592"
      #    }
      def list_customers()
        log "Entrei em list_customers"
        # define o método de envio da requisição
        method = :get

        # monta a URL de chamada da requisição
        endpoint =  get_env[:url] + EndPoints::QUERIES[:context] + EndPoints::QUERIES[:customers]

        # faz a chamada a plataforma de pagamento (MIDAS)
        request(method, endpoint, self.login, self.password, {})
      end

      #= This method performs a query by a specific customer's document type and number.
      # This is a is synchronous operation, using method GET
      #
      # Params:
      #   documentType: string (CPF/CNPJ)
      #   documentNumeber: number
      #
      # Response:
      #   result: {
      #     success: true/false
      #     code: "XXX"
      #     message: "Some message to you"
      #   }
      def transactions_by_customer(documentType='CPF', documentNumber='', status = nil)
        log "transactions_by_customer"

        # define o método de envio da requisição
        method = :get

        # monta a URL de chamada da requisição
        endpoint =  get_env[:url] + EndPoints::QUERIES[:context] + EndPoints::QUERIES[:transactions_by_customer].gsub('{documentType}', documentType).gsub('{documentNumber}', sanitized_document_number(documentNumber))

        # monta o parâmetro status na requisição da url
        endpoint = status.blank? ? endpoint.gsub("&status={status}", '') : endpoint.gsub("{status}", status)

        request(method, endpoint, login, password, {})
      end

      #= This method performs a query by a specific customer's document type and number and return all subscriptions from
      #= the user in the plataform.
      #
      # This is a is synchronous operation, using method GET
      #
      # Params:
      #   documentType: string (CPF/CNPJ)
      #   documentNumeber: number
      #
      # Response:
      #   result: {
      #     success: true/false
      #     code: "XXX"
      #     message: "Some message to you"
      #   }
      def subscriptions_by_customer(documentType='CPF', documentNumber='', status = nil)
        log "subscriptions_by_customer"

        # define o método de envio da requisição
        method = :get

        # monta a URL de chamada da requisição
        endpoint =  get_env[:url] + EndPoints::QUERIES[:context] + EndPoints::QUERIES[:subscriptions_by_customer].gsub('{documentType}', documentType).gsub('{documentNumber}', sanitized_document_number(documentNumber))

        # monta o parâmetro status na requisição da url
        endpoint = status.blank? ? endpoint.gsub("&status={status}", '') : endpoint.gsub("{status}", status)

        request(method, endpoint, login, password, {})
      end

      #= This method performs a query that summarizes all card's(CREDIT/DEBIT) operation between a date period by DAY, BRAND, PAYMENT_METHOD and STATUS.
      #
      # This is a is synchronous operation, using method GET
      #
      # Params:
      #   start_date: string (YYYY-MM-DD)
      #   end_date: string (YYYY-MM-DD)
      #
      # Response:
      #   {
      #      "result": {
      #          "success": true,
      #          "code": "000",
      #          "message": "Sucesso"
      #      },
      #          "pagging": {
      #          "limit": 6,
      #          "count": 1,
      #          "current": 1
      #      },
      #          "transactionsSummary": [
      #          {
      #              "date": "2018-01-19",
      #              "brand": "Mastercard",
      #              "paymentMethod": "CREDIT_CARD",
      #              "status": "AUTHORIZED",
      #              "count": 3,
      #              "sum": 600,
      #              "average": 200
      #          },
      #          {
      #              "date": "2018-01-19",
      #              "brand": "Mastercard",
      #              "paymentMethod": "CREDIT_CARD",
      #              "status": "CAPTURED",
      #              "count": 3,
      #              "sum": 300,
      #              "average": 100
      #          }]
      #   }
      def cards_summary_by_day(start_date=(Date.today.at_beginning_of_month).strftime('%Y-%m-%d'), end_date = Date.today.strftime('%Y-%m-%d'))
        log "Entrei em cards_summary_by_day"
        # define o método de envio da requisição
        method = :get

        # monta a URL de chamada da requisição
        endpoint =  get_env[:url] + EndPoints::QUERIES[:context] + EndPoints::QUERIES[:cards_summary_by_day]
        endpoint =  endpoint.gsub("{startDate}", start_date).gsub("{endDate}", end_date)

        # faz a chamada a plataforma de pagamento (MIDAS)
        response = request(method, endpoint, self.login, self.password, {})

        result = response[:result]
        pagging = response[:pagging]
        if response[:transactionsSummary].kind_of?(Array) || response[:transactionsSummary].blank?
          transactions = response[:transactionsSummary]
        else
          transaction_array = []
          transaction_array << response[:transactionsSummary]
          transactions = transaction_array
        end

        response[:result] = result
        response[:pagging] = pagging
        response[:transactionsSummary] = transactions

        response
      end

      #= This method performs a query that summarizes all card's(CREDIT/DEBIT) operation between a date period by by MONTH, BRAND, PAYMENT_METHOD and STATUS.
      #
      # This is a is synchronous operation, using method GET
      #
      # Params:
      #   start_month: string (MM)
      #   end_month: string (MM)
      #   start_year: string (YYYY)
      #   end_year: string (YYYY)
      #
      # Response:
      #   {
      #      "result": {
      #          "success": true,
      #          "code": "000",
      #          "message": "Sucesso"
      #      },
      #          "pagging": {
      #          "limit": 6,
      #          "count": 1,
      #          "current": 1
      #      },
      #      "transactionsSummary": [
      #       {
      #           "year": 2018,
      #           "month": 1,
      #           "brand": "Mastercard",
      #           "paymentMethod": "CREDIT_CARD",
      #           "status": "AUTHORIZED",
      #           "count": 3,
      #           "sum": 600,
      #           "average": 200
      #       },
      #       {
      #           "year": 2018,
      #           "month": 1,
      #           "brand": "Mastercard",
      #           "paymentMethod": "CREDIT_CARD",
      #           "status": "CAPTURED",
      #           "count": 3,
      #           "sum": 300,
      #           "average": 200
      #       }]
      #   }
      def cards_summary_by_month(start_month=(Date.today - 1.year).strftime('%m'), start_year=((Date.today - 1.year).strftime('%Y')), end_month=(Date.today).strftime('%m'), end_year=((Date.today).strftime('%Y')))
        log "Entrei em cards_summary_by_month"
        # define o método de envio da requisição
        method = :get

        # monta a URL de chamada da requisição
        endpoint =  get_env[:url] + EndPoints::QUERIES[:context] + EndPoints::QUERIES[:cards_summary_by_month]
        endpoint =  endpoint.gsub("{startMonth}", start_month).gsub("{startYear}", start_year).gsub("{endMonth}", end_month).gsub("{endYear}", end_year)

        # faz a chamada a plataforma de pagamento (MIDAS)
        response = request(method, endpoint, self.login, self.password, {})

        result = response[:result]
        pagging = response[:pagging]
        if response[:transactionsSummary].kind_of?(Array) || response[:transactionsSummary].blank?
          transactions = response[:transactionsSummary]
        else
          transaction_array = []
          transaction_array << response[:transactionsSummary]
          transactions = transaction_array
        end

        response[:result] = result
        response[:pagging] = pagging
        response[:transactionsSummary] = transactions

        response
      end

      #= This method performs a query that summarizes all billet's operations between a date period by DATE, BANK, PAYMENT_METHOD and STATUS.
      #
      # This is a is synchronous operation, using method GET
      #
      # Params:
      #   start_date: string (YYYY-MM-DD)
      #   end_date: string (YYYY-MM-DD)
      #
      # Response:
      #   {
      #      "result": {
      #          "success": true,
      #          "code": "000",
      #          "message": "Sucesso"
      #      },
      #          "pagging": {
      #          "limit": 6,
      #          "count": 1,
      #          "current": 1
      #      },
      #          "transactionsSummary": [
      #          {
      #              "date": "2018-01-19",
      #              "brand": "Mastercard",
      #              "paymentMethod": "CREDIT_CARD",
      #              "status": "AUTHORIZED",
      #              "count": 3,
      #              "sum": 600,
      #              "average": 200
      #          },
      #          {
      #              "date": "2018-01-19",
      #              "brand": "Mastercard",
      #              "paymentMethod": "CREDIT_CARD",
      #              "status": "CAPTURED",
      #              "count": 3,
      #              "sum": 300,
      #              "average": 100
      #          }]
      #   }
      def billets_summary_by_day(start_date=(Date.today.at_beginning_of_month).strftime('%Y-%m-%d'), end_date = Date.today.strftime('%Y-%m-%d'))
        log "Entrei em billets_summary_by_day"
        # define o método de envio da requisição
        method = :get

        # monta a URL de chamada da requisição
        endpoint =  get_env[:url] + EndPoints::QUERIES[:context] + EndPoints::QUERIES[:billets_summary_by_day]
        endpoint =  endpoint.gsub("{startDate}", start_date).gsub("{endDate}", end_date)

        # faz a chamada a plataforma de pagamento (MIDAS)
        response = request(method, endpoint, self.login, self.password, {})

        result = response[:result]
        pagging = response[:pagging]
        if response[:transactionsSummary].kind_of?(Array) || response[:transactionsSummary].blank?
          transactions = response[:transactionsSummary]
        else
          transaction_array = []
          transaction_array << response[:transactionsSummary]
          transactions = transaction_array
        end

        response[:result] = result
        response[:pagging] = pagging
        response[:transactionsSummary] = transactions

        response
      end

      #= This method performs a query that summarizes all billet's operations between a date period by MONTH, BANK, PAYMENT_METHOD and STATUS.
      #
      # This is a is synchronous operation, using method GET
      #
      # Params:
      #   start_month: string (MM)
      #   end_month: string (MM)
      #   start_year: string (YYYY)
      #   end_year: string (YYYY)
      #
      # Response:
      #   {
      #      "result": {
      #          "success": true,
      #          "code": "000",
      #          "message": "Sucesso"
      #      },
      #          "pagging": {
      #          "limit": 6,
      #          "count": 1,
      #          "current": 1
      #      },
      #      "transactionsSummary": [
      #       {
      #           "year": 2018,
      #           "month": 1,
      #           "brand": "Mastercard",
      #           "paymentMethod": "CREDIT_CARD",
      #           "status": "AUTHORIZED",
      #           "count": 3,
      #           "sum": 600,
      #           "average": 200
      #       },
      #       {
      #           "year": 2018,
      #           "month": 1,
      #           "brand": "Mastercard",
      #           "paymentMethod": "CREDIT_CARD",
      #           "status": "CAPTURED",
      #           "count": 3,
      #           "sum": 300,
      #           "average": 200
      #       }]
      #   }
      def billets_summary_by_month(start_month=(Date.today - 1.year).strftime('%m'), start_year=((Date.today - 1.year).strftime('%Y')), end_month=(Date.today).strftime('%m'), end_year=((Date.today).strftime('%Y')))
        log "Entrei em billets_summary_by_month"
        # define o método de envio da requisição
        method = :get

        # monta a URL de chamada da requisição
        endpoint =  get_env[:url] + EndPoints::QUERIES[:context] + EndPoints::QUERIES[:billets_summary_by_month]
        endpoint =  endpoint.gsub("{startMonth}", start_month).gsub("{startYear}", start_year).gsub("{endMonth}", end_month).gsub("{endYear}", end_year)

        # faz a chamada a plataforma de pagamento (MIDAS)
        response = request(method, endpoint, self.login, self.password, {})

        result = response[:result]
        pagging = response[:pagging]
        if response[:transactionsSummary].kind_of?(Array) || response[:transactionsSummary].blank?
          transactions = response[:transactionsSummary]
        else
          transaction_array = []
          transaction_array << response[:transactionsSummary]
          transactions = transaction_array
        end

        response[:result] = result
        response[:pagging] = pagging
        response[:transactionsSummary] = transactions

        response
      end
  end
end
