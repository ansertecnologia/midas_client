module MidasClient
  module EndPoints

    def production?
      if !self.environment.blank?
        self.environment.upcase == 'PRODUCTION'
      else
        (ENV['RACK_ENV'].upcase == 'PRODUCTION') || ENV['PRODUCTION'] || ENV['production']
      end
    end

    # alias for set_environment
    def set_env(e)
      self.environment=e
    end

    def get_environment
      self.get_env
    end

    # alias for get_environment
    def get_env
      self.production? ? EndPoints::PRODUCTION : EndPoints::DEVELOPMENT
    end

    DEVELOPMENT ={
        url: 'http://api.sandbox.ansertecnologia.net',
        management_auth: 'Basic bWlkYXM6bXRmYndZQDE5Nzc='
    }

    PRODUCTION ={
        url: 'https://api.ansertecnologia.com',
        management_auth: 'Basic bWlkYXM6aWNzZFA/MTk5OQ=='
    }

    OPERATIONS = {
        context: '/midas-core/v2',
        store: '/creditcard',
        synchronous_transaction: '/transaction/creditcard',
        asynchronous_transaction: '/transaction/creditcard/dispatch',
        asynchronous_debit_transaction: '/transaction/debitcard',
        authorize: '/transaction/creditcard/authorize',
        confirm: '/transaction/creditcard/{transactionToken}/capture',
        cancel: '/transaction/creditcard/{transactionToken}/cancel',
        refund: '/transaction/creditcard/{transactionToken}/refund',
        query_by_transaction: '/transaction/{transactionToken}',
        callback: 'https://checkout.express/callback',
        cards_by_user: '/customer/{documentType}/{documentNumber}/creditcard',
        create_billet: '/transaction/bankbillet/',
        print_billet: '/transaction/bankbillet/{transactionToken}'
    }

    SUBSCRIPTIONS = {
        context: '/midas-core/v2',
        by_token: '/subscription/{subscriptionToken}',
        invoices: '/subscription/{subscriptionToken}/invoices',
        invoices_renew: '/subscription/{subscriptionToken}/invoices',
        invoice_transactions: '/invoice/{invoiceToken}/transactions',
        invoice_payment: '/invoice/{invoiceToken}/pay',
        invoice_external_payment:'invoice/{invoiceToken}/pay/external',
        invoice_cancel: '/invoice/{invoiceToken}/cancel',
        subscription_transactions: '/subscription/{subscriptionToken}/transactions',
        create_manual: '/subscription/',
        create_recurrent: '/subscription/creditcard',
        cancel: '/subscription/{subscriptionToken}/cancel',
        renew: '/subscription/{subscriptionToken}/renew',
        update: '/subscription/{subscriptionToken}',
        update_card: '/subscription/{subscriptionToken}/payment-method/creditcard',
        update_invoice: '/invoice/{invoiceToken}',
        callback: 'https://checkout.express/callback',
    }

    QUERIES = {
        context: '/midas-query/v2',
        by_external_id: '/transactions/external-id/{externalId}',
        by_external_ids: '/transactions/external-ids',
        by_transaction_tokens: '/transactions/transaction-tokens',
        by_period: '/transactions?startDate={startDate}&endDate={endDate}&status={status}&type={type}',
        subscriptions: "/subscriptions?status={status}",
        invoices_by_expiration_date: '/invoices/expiration-date?startDate={startDate}&endDate={endDate}&status={status}',
        invoices_by_payment_date: '/invoices/payment-date?startDate={startDate}&endDate={endDate}&status={status}',
        creditcards: '/creditcards',
        customers: '/customers',
        transactions_by_customer: '/transactions/customer?documentType={documentType}&documentNumber={documentNumber}&status={status}',
        subscriptions_by_customer: '/subscriptions?documentType={documentType}&documentNumber={documentNumber}&status={status}',

        cards_summary_by_day: '/summary/transactions/card/daily?startDate={startDate}&endDate={endDate}',
        billets_summary_by_day: '/summary/transactions/bank/daily?startDate={startDate}&endDate={endDate}',
        cards_summary_by_month: '/summary/transactions/card/monthly?startMonth={startMonth}&startYear={startYear}&endMonth={endMonth}&endYear={endYear}',
        billets_summary_by_month: '/summary/transactions/bank/monthly?startMonth={startMonth}&startYear={startYear}&endMonth={endMonth}&endYear={endYear}',
    }

    MANAGEMENTS = {
        context: '/midas-managment/v2',
        create_pos: '/pointofsale',
        list_pos: '/pointofsale',
        pos_details: '/pointofsale/{midasLogin}'
    }

  end
end